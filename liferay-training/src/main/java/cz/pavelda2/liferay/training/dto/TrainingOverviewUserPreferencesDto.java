package cz.pavelda2.liferay.training.dto;

/**
 * Created by David Pavel on 1. 2. 2016.
 */
public class TrainingOverviewUserPreferencesDto {
    private boolean name = true;
    private boolean description;
    private boolean duration = true;
    private boolean capacity;
    private boolean beginDate = true;
    private boolean occupation;

    public boolean isName() {
        return name;
    }

    public void setName(boolean name) {
        this.name = name;
    }

    public boolean isDescription() {
        return description;
    }

    public void setDescription(boolean description) {
        this.description = description;
    }

    public boolean isDuration() {
        return duration;
    }

    public void setDuration(boolean duration) {
        this.duration = duration;
    }

    public boolean isCapacity() {
        return capacity;
    }

    public void setCapacity(boolean capacity) {
        this.capacity = capacity;
    }

    public boolean isBeginDate() {
        return beginDate;
    }

    public void setBeginDate(boolean beginDate) {
        this.beginDate = beginDate;
    }

    public boolean isOccupation() {
        return occupation;
    }

    public void setOccupation(boolean occupation) {
        this.occupation = occupation;
    }

    @Override
    public String toString() {
        return "TrainingOverviewUserPreferencesDto{" +
                "name=" + name +
                ", description=" + description +
                ", duration=" + duration +
                ", capacity=" + capacity +
                ", beginDate=" + beginDate +
                ", occupation=" + occupation +
                '}';
    }
}
