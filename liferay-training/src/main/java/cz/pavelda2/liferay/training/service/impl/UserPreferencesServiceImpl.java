package cz.pavelda2.liferay.training.service.impl;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import cz.pavelda2.liferay.training.dto.TrainingOverviewUserPreferencesDto;
import cz.pavelda2.liferay.training.entity.EmployeeEO;
import cz.pavelda2.liferay.training.exception.TrainingException;
import cz.pavelda2.liferay.training.repository.EmployeeRepository;
import cz.pavelda2.liferay.training.service.EmployeeService;
import cz.pavelda2.liferay.training.service.UserPreferencesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by David Pavel on 1. 2. 2016.
 */
@Service
@Transactional
public class UserPreferencesServiceImpl implements UserPreferencesService {

    @Autowired
    private EmployeeService employeeService;

    @Override
    public TrainingOverviewUserPreferencesDto getTrainingOverviewPreferences(String liferayUserId) throws TrainingException {
        EmployeeEO employee = employeeService.findOrCreateByLiferayId(liferayUserId);
        return UserPreferencesConversionService.convert(employee.getUserPreferences(),
                TrainingOverviewUserPreferencesDto.class);
    }

    @Override
    public void updateTrainingOverviewPreferences(
            TrainingOverviewUserPreferencesDto userPreferencesDto, String liferayUserId) throws TrainingException {
        Map<String, String> newPreferences = UserPreferencesConversionService.convert(userPreferencesDto);
        EmployeeEO employee = employeeService.findOrCreateByLiferayId(liferayUserId);
        employee.getUserPreferences().putAll(newPreferences);
        employeeService.update(employee);
    }

    private static class UserPreferencesConversionService {

        private static final String PREFERENCES_CLASS_SUFFIX = ":";

        public static<T> T convert(Map<String, String> map, Class<T> userPreferencesObjectClass) {
            try {
                T object = userPreferencesObjectClass.newInstance();
                final String prefix = userPreferencesObjectClass.getName() + PREFERENCES_CLASS_SUFFIX;
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    if (entry.getKey().startsWith(prefix)) {
                        String fieldName = entry.getKey().substring(prefix.length());
                        try {
                            Field field = TrainingOverviewUserPreferencesDto.class.getDeclaredField(fieldName);
                            field.setAccessible(true);
                            field.set(object, Boolean.valueOf(entry.getValue()));
                        } catch (NoSuchFieldException e) {
                            e.printStackTrace();
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return object;
            } catch (InstantiationException e) {
                throw new RuntimeException("Can not convert user preferences.", e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Can not convert user preferences.", e);
            }
        }
        public static Map<String, String> convert(Object userPreferences) {
            Map<String, String> map = new HashMap<String, String>();

            final String prefix = userPreferences.getClass().getName()+ PREFERENCES_CLASS_SUFFIX;

            Field[] declaredFields = TrainingOverviewUserPreferencesDto.class.getDeclaredFields();
            for (Field declaredField : declaredFields) {
                declaredField.setAccessible(true);
                try {
                    map.put(prefix + declaredField.getName(), String.valueOf(declaredField.get(userPreferences)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }

            return map;
        }
    }
}
