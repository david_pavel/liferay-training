package cz.pavelda2.liferay.training.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserServiceUtil;
import cz.pavelda2.liferay.training.entity.EmployeeEO;
import cz.pavelda2.liferay.training.exception.TrainingException;
import cz.pavelda2.liferay.training.repository.EmployeeRepository;
import cz.pavelda2.liferay.training.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by David Pavel on 3. 2. 2016.
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public EmployeeEO findOrCreateByLiferayId(String liferayUserId) throws TrainingException {
        if (liferayUserId == null) {
            throw new TrainingException("Can not create employee from user id [" + liferayUserId + "]");
        }
        EmployeeEO employee = employeeRepository.findByLiferayId(liferayUserId);
        if (employee == null) {
            try {
                // Create employee from liferay user
                User user = UserServiceUtil.getUserById(Long.parseLong(liferayUserId));
                employee = new EmployeeEO();
                employee.setLastName(user.getLastName());
                employee.setFirstName(user.getFirstName());
                employee.setLiferayId(liferayUserId);
                employeeRepository.create(employee);
            } catch (PortalException e) {
                throw new TrainingException("Can not retrieve user detail from portal for user id ["
                        + liferayUserId + "] to create employee", e);
            } catch (SystemException e) {
                throw new TrainingException("Can not create employee from user id [" + liferayUserId + "]", e);
            }
        }
        return employee;
    }

    @Override
    public void update(EmployeeEO employee) {
        employeeRepository.update(employee);
    }
}
