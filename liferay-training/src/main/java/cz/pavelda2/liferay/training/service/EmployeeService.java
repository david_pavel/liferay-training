package cz.pavelda2.liferay.training.service;

import cz.pavelda2.liferay.training.entity.EmployeeEO;
import cz.pavelda2.liferay.training.exception.TrainingException;

/**
 * Created by David Pavel on 3. 2. 2016.
 */
public interface EmployeeService {
    public EmployeeEO findOrCreateByLiferayId(String liferayUserId) throws TrainingException;

    public void update(EmployeeEO employee);
}
