package cz.pavelda2.liferay.training.web.portlet.overview;

import cz.pavelda2.liferay.training.web.portlet.TrainingCommonConstants;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
public class TrainingOverviewConstants extends TrainingCommonConstants {
    // Views
    public static final String OVERVIEW_VIEW = "training/overview/view";
    public static final String OVERVIEW_EDIT = "training/overview/edit";

    // Model attributes
    public static final String ATTRIBUTE_TRAINING_LIST = "trainings";
    public static final String ATTRIBUTE_OVERVIEW_USER_PREFERENCES = "preferences";
    public static final String ATTRIBUTE_ERROR = "error";

    public static final String ACTION_CHANGE_USER_PREFERENCES = "changeUserPreferencesAction";
}
