package cz.pavelda2.liferay.training.repository;

import java.util.List;

import cz.pavelda2.liferay.training.entity.AbstractEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
public abstract class AbstractCrudRepository<T extends AbstractEntity> implements CrudRepository<T> {

    private Class<T> clazz;
    private SessionFactory sessionFactory;

    public AbstractCrudRepository(Class<T> clazz, SessionFactory sessionFactory) {
        this.clazz = clazz;
        this.sessionFactory = sessionFactory;
    }

    public T findOne(long id) {
        return (T) getSession().get(clazz, id);
    }

    public List<T> findAll() {
        return getSession().createQuery("from " + clazz.getName()).list();
    }

    public Long create(T entity) {
        getSession().persist(entity);
        return entity.getId();
    }

    public void update(T entity) {
        getSession().merge(entity);
    }

    public void delete(T entity) {
        getSession().delete(entity);
    }

    public void deleteById(long entityId) {
        T entity = findOne(entityId);
        delete(entity);
    }

    protected final Session getSession() {
        return sessionFactory.getCurrentSession();
    }

}
