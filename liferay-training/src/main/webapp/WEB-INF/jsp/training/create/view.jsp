<%@include file="../../init.jspf" %>
<f:setBundle basename="portlet.detail.messages"/>
<%@page import="static cz.pavelda2.liferay.training.web.portlet.detail.TrainingDetailConstants.*" %>

<script src="http://cdn.alloyui.com/3.0.1/aui/aui-min.js"></script>
<link href="http://cdn.alloyui.com/3.0.1/aui-css/css/bootstrap.min.css" rel="stylesheet"/>

<div id="${ns}createView">

    <portlet:actionURL var="createTrainingAction" name="<%= ACTION_CREATE_TRAINING %>"/>

    <div class="row-fluid">
        <form:form commandName="<%= ATTRIBUTE_TRAINING %>" action="${createTrainingAction}" class="form-horizontal" method="POST">
            <fieldset>
                <legend>
                    <spring:message code="training.create"/>
                </legend>
                <!-- Name -->
                <spring:bind path="name">
                    <div class="control-group ${status.error ? 'error' : (not empty status.value ? 'success' : '')}">
                        <form:label path="name" class="control-label" ><f:message key="training.name" /></form:label>
                        <div class="controls">
                            <form:input path="name" type="text" class="input-medium" />
                            <span class="help-inline"><form:errors path="name"/></span>
                        </div>
                    </div>
                </spring:bind>
                <!-- Capacity -->
                <spring:bind path="capacity">
                    <div class="control-group ${status.error ? 'error' : (not empty status.value ? 'success' : '')}">
                        <form:label path="capacity" class="control-label" ><f:message key="training.capacity" /></form:label>
                        <div class="controls">
                            <form:input path="capacity" type="text" class="input-medium" />
                            <span class="help-inline"><form:errors path="capacity"/></span>
                        </div>
                    </div>
                </spring:bind>
                <!-- Begin Date -->
                <spring:bind path="beginDate">
                    <div class="control-group ${status.error ? 'error' : (not empty status.value ? 'success' : '')}">
                        <form:label path="beginDate" class="control-label" ><f:message key="training.beginDate" /></form:label>
                        <div class="controls">
                            <form:input id="${ns}beginDate" path="beginDate" type="text" class="input-medium" autocomplete="false" />
                            <span class="help-inline"><form:errors path="beginDate"/></span>
                        </div>
                    </div>
                </spring:bind>
                <!-- Duration -->
                <spring:bind path="duration">
                    <div class="control-group ${status.error ? 'error' : (not empty status.value ? 'success' : '')}">
                        <form:label path="duration" class="control-label" ><f:message key="training.duration" /> (<f:message key="common.minutes.abbrev" />)</form:label>
                        <div class="controls">
                            <form:input path="duration" type="text" class="input-medium" />
                            <span class="help-inline"><form:errors path="duration"/></span>
                        </div>
                    </div>
                </spring:bind>
                <!-- Description -->
                <spring:bind path="description">
                    <div class="control-group ${status.error ? 'error' : (not empty status.value ? 'success' : '')}">
                        <form:label path="description" class="control-label"><f:message key="training.description" /></form:label>
                        <div class="controls">
                            <form:textarea path="description" />
                            <span class="help-inline"><form:errors path="description"/></span>
                        </div>
                    </div>
                </spring:bind>
                <!-- Controls -->
                <div class="control-group">
                    <div class="controls">
                        <input type="submit" class="btn btn-primary" value="<f:message key="training.create" />"/>
                    </div>
                </div>
            </fieldset>
        </form:form>
    </div>
    <script type="application/javascript">
        YUI().use(
                'aui-datepicker',
                function(Y) {
                    new Y.DatePicker(
                            {
                                trigger: '#<portlet:namespace/>beginDate',
                                popover: {
                                    zIndex: 1001
                                },
                                on: {
                                    selectionChange: function(event) {
                                        console.log(event.newSelection)
                                    }
                                },
                                mask: '%d.%m.%Y'
                            }
                    );
                }
        );
    </script>
</div>
