package cz.pavelda2.liferay.training.dto;

import java.util.Set;

import cz.pavelda2.liferay.training.entity.EmployeeEO;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by David Pavel on 18. 1. 2016.
 */
public class TrainingDto {
    Long id;
    String name;
    @DateTimeFormat(pattern="dd.MM.yyyy")
    DateTime beginDate;
    Integer duration;
    String description;
    Integer capacity;
    Boolean loggedIn;
    Set<EmployeeEO> employees;

    public Long getId() {
        return id;
    }

    public void setId(Long trainingId) {
        this.id = trainingId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DateTime getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(DateTime beginDate) {
        this.beginDate = beginDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Set<EmployeeEO> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<EmployeeEO> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "TrainingDto{" +
                "name='" + name + '\'' +
                ", beginDate=" + beginDate +
                ", duration=" + duration +
                ", description='" + description + '\'' +
                ", capacity=" + capacity +
                ", loggedIn=" + loggedIn +
                ", employees=" + employees +
                '}';
    }
}
