package cz.pavelda2.liferay.training.repository;

import java.util.List;

import cz.pavelda2.liferay.training.entity.AbstractEntity;

public interface CrudRepository<T extends AbstractEntity> {

    T findOne(final long id);

    List<T> findAll();

    Long create(final T entity);

    void update(final T entity);

    void delete(final T entity);

    void deleteById(final long entityId);
}