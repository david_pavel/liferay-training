package cz.pavelda2.liferay.training.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
@Entity
@Table(name = "t_training")
public class TrainingEO extends AbstractEntity {

    @Column(length = 40)
    String name;
    DateTime beginDate;
    Integer duration;
    Integer capacity;
    @Column(length = 2048)
    String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "t_training_empolyee",
            joinColumns = {@JoinColumn(name = "training_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "employee_id", referencedColumnName = "id")})
    Set<EmployeeEO> employees = new HashSet<EmployeeEO>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DateTime getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(DateTime beginDate) {
        this.beginDate = beginDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Set<EmployeeEO> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<EmployeeEO> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "TrainingEO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", beginDate=" + beginDate +
                ", duration=" + duration +
                ", description='" + description + '\'' +
                ", capacity=" + capacity +
                '}';
    }
}
