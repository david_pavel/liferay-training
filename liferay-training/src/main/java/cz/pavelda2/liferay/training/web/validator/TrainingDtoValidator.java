package cz.pavelda2.liferay.training.web.validator;

import cz.pavelda2.liferay.training.dto.TrainingDto;
import cz.pavelda2.liferay.training.entity.TrainingEO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
@Component
public class TrainingDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return TrainingDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "err.training.null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "beginDate", "err.training.null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "duration", "err.training.null");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "capacity", "err.training.null");

        TrainingDto training = (TrainingDto) target;
        if (training.getCapacity() != null && training.getCapacity() < 0) {
            errors.rejectValue("capacity", "err.training.negative");
        }
        if (training.getDuration() != null && training.getDuration() < 0) {
            errors.rejectValue("duration", "err.training.negative");
        }

    }
}
