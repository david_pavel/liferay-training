<%@include file="../../init.jspf" %>
<f:setBundle basename="portlet.overview.messages"/>
<%@page import="static cz.pavelda2.liferay.training.web.portlet.overview.TrainingOverviewConstants.*" %>


<portlet:actionURL name="<%= ACTION_CHANGE_USER_PREFERENCES %>" portletMode="EDIT" var="changeUserPreferencesAction" />

<!-- HTML Content -->
<div id="${ns}overviewView">
    <div class="row-fluid">
        <form:form commandName="<%= ATTRIBUTE_OVERVIEW_USER_PREFERENCES %>"
                   action="${changeUserPreferencesAction}" class="form-horizontal" method="POST">
            <fieldset>
                <legend>
                    <f:message key="common.columns.visible" />
                </legend>
                <div class="control-group">
                    <label class="checkbox"><form:checkbox path="name"/><f:message key="training.name"/></label>
                </div>
                <div class="control-group">
                    <label class="checkbox"><form:checkbox path="beginDate"/><f:message key="training.beginDate"/></label>
                </div>
                <div class="control-group">
                    <label class="checkbox"><form:checkbox path="duration"/><f:message key="training.duration"/></label>
                </div>
                <div class="control-group">
                    <label class="checkbox"><form:checkbox path="description"/><f:message key="training.description"/></label>
                </div>
                <div class="control-group">
                    <label class="checkbox"><form:checkbox path="occupation"/><f:message key="training.occupation"/></label>
                </div>
                <div class="control-group">
                    <label class="checkbox"><form:checkbox path="capacity"/><f:message key="training.capacity"/></label>
                </div>
                <!-- Controls -->
                <div class="control-group">
                    <div class="controls">
                        <input type="submit" class="btn btn-primary" value="<f:message key="userPreferences.change" />"/>
                    </div>
                </div>
            </fieldset>
        </form:form>
    </div>
</div>