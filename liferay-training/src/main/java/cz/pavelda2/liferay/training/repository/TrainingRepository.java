package cz.pavelda2.liferay.training.repository;

import cz.pavelda2.liferay.training.entity.TrainingEO;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
public interface TrainingRepository extends CrudRepository<TrainingEO> {

    public Boolean hasUserTraining(Long trainingId, String liferayUserId);
}
