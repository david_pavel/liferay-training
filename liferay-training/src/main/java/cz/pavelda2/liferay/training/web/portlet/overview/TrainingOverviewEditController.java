package cz.pavelda2.liferay.training.web.portlet.overview;

import static cz.pavelda2.liferay.training.web.portlet.overview.TrainingOverviewConstants.*;

import javax.portlet.*;

import com.liferay.portal.kernel.util.ParamUtil;
import cz.pavelda2.liferay.training.dto.TrainingOverviewUserPreferencesDto;
import cz.pavelda2.liferay.training.exception.TrainingException;
import cz.pavelda2.liferay.training.service.UserPreferencesService;
import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller
@RequestMapping("EDIT")
public class TrainingOverviewEditController {
    private static final Logger LOG = Logger.getLogger(TrainingOverviewEditController.class);;

    @Autowired
    private UserPreferencesService userPreferencesService;

    @RenderMapping
    public String userPreferencesRender(RenderRequest request, Model model) throws TrainingException {
        ParamUtil.print(request);
        if (!model.containsAttribute(ATTRIBUTE_OVERVIEW_USER_PREFERENCES)) {
            TrainingOverviewUserPreferencesDto userPreferences = userPreferencesService.getTrainingOverviewPreferences(request.getRemoteUser());
            model.addAttribute(ATTRIBUTE_OVERVIEW_USER_PREFERENCES, userPreferences);
        }
        return OVERVIEW_EDIT;
    }

    @ActionMapping(ACTION_CHANGE_USER_PREFERENCES)
    public void changeUserPreferencesAction(
            @ModelAttribute(ATTRIBUTE_OVERVIEW_USER_PREFERENCES) TrainingOverviewUserPreferencesDto userPreferences,
            ActionRequest request, ActionResponse response) throws PortletModeException, TrainingException {
        LogMF.info(LOG, "Changing user preferences {0}", userPreferences);
        userPreferencesService.updateTrainingOverviewPreferences(userPreferences, request.getRemoteUser());
        response.setPortletMode(PortletMode.VIEW);
    }

}