package cz.pavelda2.liferay.training.web.portlet;

/**
 * Created by David Pavel on 18. 1. 2016.
 */
public class TrainingCommonConstants {

    // Params
    public static final String PARAM_TRAINING_ID = "trainingId";
    public static final String PARAM_ASSIGN_TRAINING_ID = "assignTrainingId";
    public static final String PARAM_TRAINING_VIEW = "trainingView";
    public static final String VIEW_TRAINING_CREATE = "CREATE";
    public static final String VIEW_TRAINING_DETAIL = "DETAIL";

    // Actions
    public static final String ACTION_CREATE_TRAINING = "createTrainingAction";
    public static final String ACTION_ASSIGN_TRAINING = "assignTrainingAction";

    // Common
    public static final String DATE_FORMAT = "dd.MM.yyyy";
    public static final String MESSAGE_CODE_TRAINING_CAPACITY_REACHED = "training.capacity.reached";

    // View
    public static final String EXCEPTION_VIEW = "training/exception/view";
}
