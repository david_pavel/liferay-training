<%@include file="../../init.jspf" %>
<f:setBundle basename="portlet.overview.messages"/>
<%@page import="static cz.pavelda2.liferay.training.web.portlet.overview.TrainingOverviewConstants.*" %>

<!-- HTML Content -->
<div id="${ns}overviewView">
    <c:if test="${not empty error}">
        <div class="alert alert-error">
            <i class="icon-remove"></i>&nbsp;<c:out value="${error}"/>
        </div>
    </c:if>
    <h3><f:message key="category.training" /></h3>
    <div class="row-fluid">
        <table class="table table-striped table-hover table-condensed">
            <thead>
            <tr>
                <th></th>
                <c:if test="${preferences.name}"><th><f:message key="training.name"/></th></c:if>
                <c:if test="${preferences.beginDate}"><th><f:message key="training.beginDate"/></th></c:if>
                <c:if test="${preferences.duration}"><th><f:message key="training.duration"/></th></c:if>
                <c:if test="${preferences.occupation}"><th><f:message key="training.occupation"/></th></c:if>
                <c:if test="${preferences.capacity}"><th><f:message key="training.capacity"/></th></c:if>
                <c:if test="${preferences.description}"><th><f:message key="training.description"/></th></c:if>
                <th><f:message key="common.actions"/></th>
            </tr>
            </thead>
            <tbody>
                <c:forEach var="training" items="${trainings}">
                    <tr>
                        <td>
                            <c:if test="${training.loggedIn}"><i class="text-success icon-ok"></i></c:if>
                        </td>
                        <c:if test="${preferences.name}">
                            <td><c:out value="${training.name}"/> (<c:out value="${training.id}"/>)</td>
                        </c:if>
                        <c:if test="${preferences.beginDate}">
                            <td><c:out value="${training.beginDate.toString('dd.MM.yyyy hh:mm')}"/></td>
                        </c:if>
                        <c:if test="${preferences.duration}">
                            <td><c:out value="${training.duration}"/>&nbsp;<f:message key="common.minutes.abbrev"/></td>
                        </c:if>
                        <c:if test="${preferences.occupation}">
                            <td><c:out value="${training.employees.size()}"/></td>
                        </c:if>
                        <c:if test="${preferences.capacity}">
                            <td><c:out value="${training.capacity}"/></td>
                        </c:if>
                        <c:if test="${preferences.description}">
                            <td><c:out value="${training.description}"/></td>
                        </c:if>
                        <td>
                            <div class="btn-group">
                                <portlet:renderURL var="detailTrainingURL">
                                    <portlet:param name="<%= PARAM_TRAINING_VIEW %>" value="<%= VIEW_TRAINING_DETAIL %>"/>
                                    <portlet:param name="<%=PARAM_TRAINING_ID%>" value="${training.id}"/>
                                </portlet:renderURL>
                                <a class="btn btn-small btn-info" href="${detailTrainingURL}"><f:message key="training.detail"/></a>
                                <portlet:actionURL var="assignTrainingURL" name="<%= ACTION_ASSIGN_TRAINING %>" >
                                    <portlet:param name="<%=PARAM_ASSIGN_TRAINING_ID%>" value="${training.id}"/>
                                </portlet:actionURL>
                                <a href="${assignTrainingURL}" class="btn btn-small btn-${training.loggedIn ? 'danger' : 'success'}">
                                    <f:message key="training.${training.loggedIn ? 'cancel' : 'assign'}"/>
                                </a>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <p class="text-center"><f:message key="training.count"/>: <strong><c:out value="${trainings.size()}"/></strong></p>
    </div>
    <div class="row-fluid">
        <portlet:renderURL var="createTrainingURL">
            <portlet:param name="<%= PARAM_TRAINING_VIEW %>" value="<%= VIEW_TRAINING_CREATE %>"/>
        </portlet:renderURL>
        <a class="btn btn-default" href="${createTrainingURL}"><f:message key="training.create"/></a>
    </div>
</div>