package cz.pavelda2.liferay.training.repository.impl;

import cz.pavelda2.liferay.training.entity.EmployeeEO;
import cz.pavelda2.liferay.training.repository.AbstractCrudRepository;
import cz.pavelda2.liferay.training.repository.EmployeeRepository;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
@Repository
public class EmployeeRepositoryImpl extends AbstractCrudRepository<EmployeeEO> implements EmployeeRepository {

    @Autowired
    public EmployeeRepositoryImpl(SessionFactory sessionFactory) {
        super(EmployeeEO.class, sessionFactory);
    }

    @Override
    public EmployeeEO findByLiferayId(String currentUserId) {
        return (EmployeeEO) getSession().createCriteria(EmployeeEO.class)
                .add(Restrictions.eq("liferayId", currentUserId))
                .uniqueResult();
    }
}
