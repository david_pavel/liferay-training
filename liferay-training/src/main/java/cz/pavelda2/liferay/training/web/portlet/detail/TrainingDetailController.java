package cz.pavelda2.liferay.training.web.portlet.detail;

import static cz.pavelda2.liferay.training.web.portlet.TrainingCommonConstants.*;
import static cz.pavelda2.liferay.training.web.portlet.detail.TrainingDetailConstants.ATTRIBUTE_TRAINING;
import static cz.pavelda2.liferay.training.web.portlet.detail.TrainingDetailConstants.TRAINING_DTO;
import static cz.pavelda2.liferay.training.web.portlet.overview.TrainingOverviewConstants.ATTRIBUTE_ERROR;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;

import java.util.Locale;

import com.liferay.portal.kernel.util.ParamUtil;
import cz.pavelda2.liferay.training.dto.TrainingDto;
import cz.pavelda2.liferay.training.exception.TrainingException;
import cz.pavelda2.liferay.training.service.TrainingService;
import cz.pavelda2.liferay.training.web.helper.JodaDateTimeEditor;
import cz.pavelda2.liferay.training.web.validator.TrainingDtoValidator;
import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
@Controller
@RequestMapping("VIEW")
public class TrainingDetailController {
    protected final Logger LOG = Logger.getLogger(TrainingDetailController.class);

    @Autowired
    private TrainingService service;
    @Autowired
    private TrainingDtoValidator trainingValidator;
    @Autowired
    private MessageSource messageSource;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        LOG.info("Initializing bindings for Date fields");
        binder.registerCustomEditor(DateTime.class, "beginDate", new JodaDateTimeEditor(DATE_FORMAT, true));
    }


    @RenderMapping
    public String defaultRender(Model model, RenderRequest request) {
        LogMF.info(LOG, "Showing empty training detail.", null);
        ParamUtil.print(request);
        model.addAttribute(TRAINING_DTO, null);
        return TrainingDetailConstants.DETAIL_VIEW;
    }

    @RenderMapping(params = PARAM_TRAINING_VIEW + "=" + VIEW_TRAINING_DETAIL)
    public String detTrainingFormRender(@RequestParam(name = PARAM_TRAINING_ID, required = false) Long trainingId,
                                        Model model, RenderRequest request) {
        LogMF.info(LOG, "Showing training detail {0}.", trainingId);
        ParamUtil.print(request);
        if (trainingId == null) {
            model.addAttribute(TRAINING_DTO, null);
        } else {
            model.addAttribute(TRAINING_DTO, service.getTraining(trainingId, request.getRemoteUser()));
        }
        return TrainingDetailConstants.DETAIL_VIEW;
    }

    @RenderMapping(params = PARAM_TRAINING_VIEW + "=" + VIEW_TRAINING_CREATE)
    public String createTrainingFormRender(Model model, RenderRequest request) {
        LogMF.info(LOG, "Showing create form for training.", null);
        ParamUtil.print(request);
        if (!model.containsAttribute(ATTRIBUTE_TRAINING)) {
            model.addAttribute(ATTRIBUTE_TRAINING, new TrainingDto());
        }
        return TrainingDetailConstants.CREATE_FORM_VIEW;
    }

    @ActionMapping(ACTION_CREATE_TRAINING)
    public void createTrainingAction(
            @ModelAttribute(ATTRIBUTE_TRAINING) TrainingDto training,
            BindingResult result, ActionRequest request, Model model,
            ActionResponse response) {
        LogMF.info(LOG, "Creating training {0}", training);
        ParamUtil.print(request);
        trainingValidator.validate(training, result);
        if (!result.hasErrors()) {
            LogMF.info(LOG, "Saving training object: {0}.", training);
            Long trainingId = service.createNewTraining(training);
            model.asMap().remove(ATTRIBUTE_TRAINING);
            response.setRenderParameter(PARAM_TRAINING_VIEW, VIEW_TRAINING_DETAIL);
            response.setRenderParameter(PARAM_TRAINING_ID, String.valueOf(trainingId));
        } else {
            LogMF.info(LOG, "Training object has errors: {0}.", training);
        }
    }

    @ActionMapping(ACTION_ASSIGN_TRAINING)
    public void assignTrainingAction(
            @RequestParam(value = PARAM_ASSIGN_TRAINING_ID) Long trainingId,
            Model model, Locale locale,
            ActionRequest request, ActionResponse response) {
        LogMF.info(LOG, "Assigning training by id {0}", trainingId);
        try {
            boolean success = service.changeAssignment(trainingId, request.getRemoteUser());
            if (success) {
                response.setRenderParameter(PARAM_TRAINING_VIEW, VIEW_TRAINING_DETAIL);
                response.setRenderParameter(PARAM_TRAINING_ID, String.valueOf(trainingId));
            } else {
                model.addAttribute(ATTRIBUTE_ERROR,
                messageSource.getMessage(MESSAGE_CODE_TRAINING_CAPACITY_REACHED, new Object[0], locale));
            }
        } catch (TrainingException e) {
            e.printStackTrace();
        }
    }

}
