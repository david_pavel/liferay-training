<%@include file="../../init.jspf" %>
<f:setBundle basename="portlet.detail.messages"/>
<%@page import="static cz.pavelda2.liferay.training.web.portlet.detail.TrainingDetailConstants.*" %>

<script src="http://cdn.alloyui.com/3.0.1/aui/aui-min.js"></script>
<link href="http://cdn.alloyui.com/3.0.1/aui-css/css/bootstrap.min.css" rel="stylesheet"/>

<div id="${ns}exceptionView">
    <div class="alert alert-error">
        <strong><f:message key="exception.page.header"/> </strong>
        <f:message key="exception.page.message"/>
    </div>
</div>
