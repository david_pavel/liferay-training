package cz.pavelda2.liferay.training.service;

import cz.pavelda2.liferay.training.dto.TrainingOverviewUserPreferencesDto;
import cz.pavelda2.liferay.training.exception.TrainingException;

/**
 * Created by David Pavel on 1. 2. 2016.
 */
public interface UserPreferencesService {
    public TrainingOverviewUserPreferencesDto getTrainingOverviewPreferences(String liferayUserId) throws TrainingException;
    public void updateTrainingOverviewPreferences(TrainingOverviewUserPreferencesDto userPreferencesDto, String liferayUserId) throws TrainingException;
}
