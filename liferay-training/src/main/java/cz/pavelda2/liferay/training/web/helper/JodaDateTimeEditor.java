package cz.pavelda2.liferay.training.web.helper;

import java.beans.PropertyEditorSupport;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.util.StringUtils;

public class JodaDateTimeEditor extends PropertyEditorSupport {

    private final String pattern;
    private final boolean allowEmpty;

    /**
     * Create a new CustomDateTimeEditor instance.
     * <p/>
     * The "allowEmpty" parameter states if an empty String should be
     * allowed for
     * parsing, i.e. get interpreted as null value. Otherwise, an
     * IllegalArgumentException gets thrown.
     *
     * @param allowEmpty if empty strings should be allowed
     */
    public JodaDateTimeEditor(String pattern,
                              boolean allowEmpty) {
        this.pattern = pattern;
        this.allowEmpty = allowEmpty;
    }

    @Override
    public String getAsText() {
        if (getValue() == null) return "";

        DateTime value = (DateTime) getValue();
        return value != null ? value.toString(pattern) : "";
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (!StringUtils.hasText(text)) {
            if (allowEmpty) {
                setValue(null);
            } else {
                throw new IllegalArgumentException(
                        "The specified DateTime string cannot be null");
            }
        } else {
            setValue(DateTimeFormat.forPattern(pattern).parseDateTime(text));
        }
    }

} 