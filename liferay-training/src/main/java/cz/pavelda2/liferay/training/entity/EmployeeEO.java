package cz.pavelda2.liferay.training.entity;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
@Entity
@Table(name = "t_employee")
public class EmployeeEO extends AbstractEntity {

    @Column(length = 100)
    String firstName;
    @Column(length = 100)
    String lastName;

    @Column(unique = true)
    String liferayId;

    @ManyToMany(mappedBy = "employees", fetch = FetchType.LAZY)
    List<TrainingEO> trainings;

    @ElementCollection
    @MapKeyColumn(name="name")
    @Column(name="value")
    @CollectionTable(name="t_user_preferences", joinColumns=@JoinColumn(name="employee_id"))
    private Map<String, String> userPreferences = new HashMap<String, String>();



    public String getLiferayId() {
        return liferayId;
    }

    public void setLiferayId(String liferayId) {
        this.liferayId = liferayId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<TrainingEO> getTrainings() {
        return trainings;
    }

    public void setTrainings(List<TrainingEO> trainings) {
        this.trainings = trainings;
    }

    public Map<String, String> getUserPreferences() {
        return userPreferences;
    }

    public void setUserPreferences(Map<String, String> userPreferences) {
        this.userPreferences = userPreferences;
    }

    @Override
    public String toString() {
        return "EmployeeEO{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", liferayId='" + liferayId + '\'' +
                ", trainings=" + trainings +
                '}';
    }
}
