package cz.pavelda2.liferay.training.repository.impl;

import cz.pavelda2.liferay.training.entity.TrainingEO;
import cz.pavelda2.liferay.training.repository.AbstractCrudRepository;
import cz.pavelda2.liferay.training.repository.TrainingRepository;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
@Repository
public class TrainingRepositoryImpl extends AbstractCrudRepository<TrainingEO> implements TrainingRepository {

    @Autowired
    public TrainingRepositoryImpl(SessionFactory sessionFactory) {
        super(TrainingEO.class, sessionFactory);
    }

    @Override
    public Boolean hasUserTraining(Long trainingId, String liferayUserId) {
        Query query = getSession().createQuery("SELECT count(t) FROM TrainingEO t " +
                "JOIN t.employees as e " +
                "WHERE t.id = :trainingId AND e.liferayId = :liferayId")
                .setParameter("trainingId", trainingId)
                .setParameter("liferayId", liferayUserId);
        return (Long) query.uniqueResult() > 0;
    }
}
