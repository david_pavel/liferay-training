package cz.pavelda2.liferay.training.service;

import cz.pavelda2.liferay.training.entity.AbstractEntity;
import cz.pavelda2.liferay.training.entity.EmployeeEO;
import cz.pavelda2.liferay.training.entity.TrainingEO;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by David Pavel on 18. 1. 2016.
 */
@Service
public class DatabasePopulator {
    public static final Logger LOG = Logger.getLogger(DatabasePopulator.class);

    private SessionFactory sessionFactory;

    private EmployeeEO eJanNovak;
    private EmployeeEO eTomasDvorak;
    private EmployeeEO eJanaNovotna;

    private TrainingEO tJava;
    private TrainingEO tLiferay;

    @Autowired
    public DatabasePopulator(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void populate() {
        LOG.info("Populating DB ...");
        createTrainings();
        createEmployees();
        createAssignments();
        LOG.info("DB populated.");
    }

    private void createAssignments() {
        tJava.getEmployees().add(eJanNovak);
        tJava.getEmployees().add(eTomasDvorak);
        tJava.getEmployees().add(eJanaNovotna);
        tLiferay.getEmployees().add(eJanaNovotna);
    }

    private void createEmployees() {
        eJanNovak = createEmployee("Jan", "Novák", "101010");
        eTomasDvorak = createEmployee("Tomáš", "Dvořák", "101011");
        eJanaNovotna = createEmployee("Jana", "Novotná", "101012");
    }

    private EmployeeEO createEmployee(String name, String surname, String liferayUserId) {
        EmployeeEO employee = new EmployeeEO();
        employee.setLiferayId(liferayUserId);
        employee.setFirstName(name);
        employee.setLastName(surname);
        save(employee);
        return employee;
    }

    private void createTrainings() {
        tJava = createTraining("JavaEE", new DateTime(2015, 2, 3, 10, 00), 60, 10, "Školení základů Java Enterprise Edition.");
        tLiferay = createTraining("Liferay I.", new DateTime(2016, 2, 3, 10, 00), 120, 20, "Základy Liferay.");
        createTraining("Liferay II.", new DateTime(2016, 12, 12, 12, 30), 180, 20, "Vývoj portletů v Liferay.");
    }

    private TrainingEO createTraining(String name, DateTime beginDate, int duration, int capacity, String description) {
        TrainingEO trainingEO = new TrainingEO();
        trainingEO.setName(name);
        trainingEO.setBeginDate(beginDate);
        trainingEO.setDuration(duration);
        trainingEO.setCapacity(capacity);
        trainingEO.setDescription(description);
        save(trainingEO);
        return trainingEO;
    }

    private void save(AbstractEntity entity) {
        sessionFactory.getCurrentSession().save(entity);
    }
}
