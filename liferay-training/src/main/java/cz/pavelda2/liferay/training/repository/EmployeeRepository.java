package cz.pavelda2.liferay.training.repository;

import cz.pavelda2.liferay.training.entity.EmployeeEO;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
public interface EmployeeRepository extends CrudRepository<EmployeeEO> {

    public EmployeeEO findByLiferayId(String currentUserId);
}
