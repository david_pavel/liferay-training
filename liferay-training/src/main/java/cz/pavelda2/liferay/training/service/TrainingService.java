package cz.pavelda2.liferay.training.service;

import java.util.List;

import com.liferay.portal.model.User;
import cz.pavelda2.liferay.training.dto.TrainingDto;
import cz.pavelda2.liferay.training.entity.TrainingEO;
import cz.pavelda2.liferay.training.exception.TrainingException;
import org.springframework.dao.DataAccessException;

/**
 * Created by David Pavel on 18. 1. 2016.
 */
public interface TrainingService {
    public Long createNewTraining(TrainingDto training);

    public List<TrainingDto> getAllTrainings(String currentUserId);

    public TrainingDto getTraining(Long id, String currentUserId);

    /**
     * Change training assignment for liferay user
     * @param trainingId id of training
     * @param currentUserId liferay id of user
     * @return true if assignment changed successfully
     * @throws TrainingException when training by trainingId not found
     */
    public boolean changeAssignment(Long trainingId, String currentUserId) throws DataAccessException, TrainingException;
}
