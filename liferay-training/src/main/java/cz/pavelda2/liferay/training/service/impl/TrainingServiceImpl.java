package cz.pavelda2.liferay.training.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cz.pavelda2.liferay.training.dto.TrainingDto;
import cz.pavelda2.liferay.training.entity.EmployeeEO;
import cz.pavelda2.liferay.training.entity.TrainingEO;
import cz.pavelda2.liferay.training.exception.TrainingException;
import cz.pavelda2.liferay.training.repository.TrainingRepository;
import cz.pavelda2.liferay.training.service.EmployeeService;
import cz.pavelda2.liferay.training.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by David Pavel on 18. 1. 2016.
 */
@Service
@Transactional
public class TrainingServiceImpl implements TrainingService {

    @Autowired
    private TrainingRepository trainingRepo;
    @Autowired
    private EmployeeService employeeService;

    @Override
    public Long createNewTraining(TrainingDto training) {
        return trainingRepo.create(TrainingConversionUtils.trainingEO(training));
    }

    @Override
    public List<TrainingDto> getAllTrainings(String currentUserId) {
        List<TrainingDto> trainingDtos = TrainingConversionUtils.trainingDto(trainingRepo.findAll(), currentUserId);
        System.out.println(trainingDtos);
        return trainingDtos;
    }

    @Override
    public TrainingDto getTraining(Long id, String currentUserId) {
        TrainingDto trainingDto = TrainingConversionUtils.trainingDto(trainingRepo.findOne(id), currentUserId);
        System.out.println(trainingDto);
        return trainingDto;
    }


    @Override
    public boolean changeAssignment(Long trainingId, String currentUserId) throws TrainingException {
        TrainingEO training = trainingRepo.findOne(trainingId);
        if (training == null) {
            throw new TrainingException("Can not find training by id: " + trainingId);
        }

        EmployeeEO employee = employeeService.findOrCreateByLiferayId(currentUserId);
        if (TrainingConversionUtils.containsUser(training.getEmployees(),currentUserId)) {
            // Cancel assignment
            training.getEmployees().remove(employee);
        } else {
            // Assign
            if (training.getEmployees().size() >= training.getCapacity()) {
                // max capacity reached
                return false;
            }
            training.getEmployees().add(employee);
        }
        trainingRepo.update(training);
        return true;
    }

    static class TrainingConversionUtils {
        public static TrainingDto trainingDto(TrainingEO training, String currentUserLiferayId) {
            TrainingDto trainingDto = new TrainingDto();
            trainingDto.setId(training.getId());
            trainingDto.setName(training.getName());
            trainingDto.setCapacity(training.getCapacity());
            trainingDto.setBeginDate(training.getBeginDate());
            trainingDto.setDuration(training.getDuration());
            trainingDto.setDescription(training.getDescription());
            trainingDto.setEmployees(training.getEmployees());
            trainingDto.setLoggedIn(containsUser(trainingDto.getEmployees(), currentUserLiferayId));
            return trainingDto;
        }

        public static List<TrainingDto> trainingDto(List<TrainingEO> trainings, String currentUserLiferayId) {
            List<TrainingDto> trainingDTOs = new ArrayList<TrainingDto>(trainings.size());
            for (TrainingEO training : trainings) {
                trainingDTOs.add(trainingDto(training, currentUserLiferayId));
            }
            return trainingDTOs;
        }

        public static Boolean containsUser(Set<EmployeeEO> employees, String currentUserLiferayId) {
            for (EmployeeEO employee : employees) {
                if (employee.getLiferayId().equals(currentUserLiferayId)) {
                    return true;
                }
            }
            return false;
        }

        public static TrainingEO trainingEO(TrainingDto training) {
            TrainingEO trainingEO = new TrainingEO();
            trainingEO.setId(training.getId());
            trainingEO.setName(training.getName());
            trainingEO.setCapacity(training.getCapacity());
            trainingEO.setBeginDate(training.getBeginDate());
            trainingEO.setDuration(training.getDuration());
            trainingEO.setDescription(training.getDescription());
            trainingEO.setEmployees(training.getEmployees());
            return trainingEO;
        }
    }
}
