<%@include file="../../init.jspf" %>
<f:setBundle basename="portlet.detail.messages"/>
<%@page import="static cz.pavelda2.liferay.training.web.portlet.detail.TrainingDetailConstants.*" %>

<div id="${ns}detailView">
    <c:if test="${not empty error}">
        <div class="alert alert-error">
            <i class="icon-remove"></i>&nbsp;<c:out value="${error}"/>
        </div>
    </c:if>
    <c:choose>
        <c:when test="${empty training}">
            <p><spring:message code="training.display.none"/></p>
            <c:out value="${params}"/>
        </c:when>
        <c:otherwise>
            <div class="row-fluid">
                <h3><c:if test="${training.loggedIn}"><i class="text-success icon-ok"></i></c:if>&nbsp;<c:out value="${training.name}"/></h3>
                <hr/>
                <h5><f:message key="training.detail"/></h5>
            </div>
            <div class="row-fluid">
                <div class="span4 text-left"><i class="icon-calendar"></i>&nbsp;<spring:message code="training.beginDate"/>:</div>
                <p class="span8"><c:out value="${training.beginDate.toString('dd.MM.yyyy hh:mm')}"/></p>
            </div>
            <div class="row-fluid">
                <div class="span4 text-left"><i class="icon-time"></i>&nbsp;<spring:message code="training.duration"/>:</div>
                <p class="span8"><c:out value="${training.duration}"/>&nbsp;<f:message key="common.minutes.abbrev"/></p>
            </div>
            <div class="row-fluid">
                <div class="span4 text-left"><i class="icon-user"></i>&nbsp;<spring:message code="training.assignedEmployees.count"/>:</div>
                <p class="span8"><c:out value="${training.employees.size()}"/>/<c:out value="${training.capacity}"/></p>
            </div>
            <div class="row-fluid">
                <div class="span4 text-left"><i class="icon-align-justify"></i>&nbsp;<spring:message code="training.description"/>:</div>
                <p class="span8"><c:out value="${training.description}"/></p>
            </div>
            <hr/>
            <div class="row-fluid">
                <h5><f:message key="training.assignedEmployees"/></h5>
                <ol>
                    <c:forEach var="empl" items="${training.employees}">
                        <li><c:out value="${empl.lastName != null ? ''.concat(empl.firstName).concat(' ').concat(empl.lastName) : empl.liferayId}"/></li>
                    </c:forEach>
                </ol>
                <c:if test="${empty training.employees}">
                    <p><spring:message code="training.assignedEmployees.none"/></p>
                </c:if>
            </div>
            <hr/>
            <div class="row-fluid">
                <portlet:actionURL var="assignTrainingURL" name="<%= ACTION_ASSIGN_TRAINING %>" >
                    <portlet:param name="<%=PARAM_ASSIGN_TRAINING_ID%>" value="${training.id}"/>
                </portlet:actionURL>
                <a href="${assignTrainingURL}" class="btn btn-${training.loggedIn ? 'danger' : 'success'}">
                    <f:message key="training.${training.loggedIn ? 'cancel' : 'assign'}"/>
                </a>
            </div>
        </c:otherwise>
    </c:choose>
</div>
