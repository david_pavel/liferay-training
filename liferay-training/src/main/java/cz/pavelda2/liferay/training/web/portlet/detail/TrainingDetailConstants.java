package cz.pavelda2.liferay.training.web.portlet.detail;

import cz.pavelda2.liferay.training.web.portlet.TrainingCommonConstants;

/**
 * Created by David Pavel on 16. 1. 2016.
 */
public class TrainingDetailConstants extends TrainingCommonConstants {
    // Views
    public static final String DETAIL_VIEW = "training/detail/view";
    public static final String CREATE_FORM_VIEW = "training/create/view";

    // Model
    public static final String TRAINING_DTO = "training";

    public static final String ATTRIBUTE_TRAINING = "attrTraining";

}
