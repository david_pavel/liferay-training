package cz.pavelda2.liferay.training.exception;

/**
 * Created by David Pavel on 21. 1. 2016.
 */
public class TrainingException extends Exception {
    public TrainingException() {
    }

    public TrainingException(String message) {
        super(message);
    }

    public TrainingException(String message, Throwable cause) {
        super(message, cause);
    }

    public TrainingException(Throwable cause) {
        super(cause);
    }

}
