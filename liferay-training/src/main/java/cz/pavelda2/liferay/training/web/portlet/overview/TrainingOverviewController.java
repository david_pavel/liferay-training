package cz.pavelda2.liferay.training.web.portlet.overview;

import static cz.pavelda2.liferay.training.web.portlet.overview.TrainingOverviewConstants.*;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import java.util.Locale;

import com.liferay.portal.kernel.util.ParamUtil;
import cz.pavelda2.liferay.training.exception.TrainingException;
import cz.pavelda2.liferay.training.service.TrainingService;
import cz.pavelda2.liferay.training.service.UserPreferencesService;
import org.apache.log4j.LogMF;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller
@RequestMapping("VIEW")
public class TrainingOverviewController {
    private static final Logger LOG = Logger.getLogger(TrainingOverviewController.class);
    ;

    @Autowired
    private TrainingService service;

    @Autowired
    private UserPreferencesService userPreferencesService;

    @Autowired
    private MessageSource messageSource;

    @RenderMapping
    public String overviewRender(RenderRequest request, Model model) throws TrainingException {

        model.addAttribute(ATTRIBUTE_OVERVIEW_USER_PREFERENCES,
                userPreferencesService.getTrainingOverviewPreferences(request.getRemoteUser()));
        model.addAttribute(ATTRIBUTE_TRAINING_LIST, service.getAllTrainings(request.getRemoteUser()));
        return OVERVIEW_VIEW;
    }

    @ActionMapping(ACTION_ASSIGN_TRAINING)
    public void assignTrainingEvent(
            @RequestParam(value = PARAM_ASSIGN_TRAINING_ID) Long trainingId, Model model, Locale locale,
            ActionRequest request, ActionResponse response) {
        LogMF.info(LOG, "Assigning training by id {0}", trainingId);
        ParamUtil.print(request);
        try {
            boolean success = service.changeAssignment(trainingId, request.getRemoteUser());
            if (success) {
                response.setRenderParameter(PARAM_TRAINING_VIEW, VIEW_TRAINING_DETAIL);
                response.setRenderParameter(PARAM_TRAINING_ID, String.valueOf(trainingId));
            } else {
                model.addAttribute(ATTRIBUTE_ERROR,
                        messageSource.getMessage(MESSAGE_CODE_TRAINING_CAPACITY_REACHED, new Object[0], locale));
            }
        } catch (TrainingException e) {
            e.printStackTrace();
        }
    }

    @ExceptionHandler({TrainingException.class})
    public String exceptionHandler(TrainingException ex) {
        return EXCEPTION_VIEW;
    }

}